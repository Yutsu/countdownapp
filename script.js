let seconds = document.querySelector('.seconds');
let minutes = document.querySelector('.minutes');
let hours = document.querySelector('.hours');
let days = document.querySelector('.days');

const countDown = () => {
    const birthday = '16 June 2021';
    const birthdayDate = new Date(birthday);
    const currentDate = new Date();

    const Totalsecond = (birthdayDate - currentDate) / 1000;
    const day = Math.floor(Totalsecond / 3600 / 24);
    const hour = Math.floor(Totalsecond / 3600) % 24;
    const minute = Math.floor(Totalsecond / 60) % 60;
    const second = Math.floor(Totalsecond) % 60;

    days.textContent = formatZero(day);
    minutes.textContent = formatZero(minute);
    hours.textContent = formatZero(hour);
    seconds.textContent = formatZero(second);
}
const formatZero = (time) => {
    return time < 10 ? `0${time}` : `${time}`;
}

const app = () => {
    countDown();
    setInterval(countDown, 1000);
}
app();

