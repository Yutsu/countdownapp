# Countdown Application
![Design homepage of Countdown Application](./images/home.png)

## Description
This is a responsive Countdown Application of my birthday date

## Contains

Display data via grid

Date function

SetInterval function

## Technologies Used
Html

Css

Javascript

## Installation
git clone https://gitlab.com/Yutsu/countdownapp.git

## Start Server
Live server from Vscode Extension and click "Go live"

## Ressources

**Freestocks.org**

https://www.pexels.com/fr-fr/photo/brouiller-feu-d-artifice-cierge-magique-effet-bokeh-285173/
